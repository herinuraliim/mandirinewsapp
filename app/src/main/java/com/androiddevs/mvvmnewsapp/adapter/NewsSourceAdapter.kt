package com.androiddevs.mvvmnewsapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.androiddevs.mvvmnewsapp.R
import com.androiddevs.mvvmnewsapp.model.NewsSources.Source
import kotlinx.android.synthetic.main.item_source_preview.view.tvNewsCategory
import kotlinx.android.synthetic.main.item_source_preview.view.tvNewsSource
import kotlinx.android.synthetic.main.item_source_preview.view.tvUrl


class NewsSourceAdapter : RecyclerView.Adapter<NewsSourceAdapter.NewsSourceViewHolder>(){
    inner class NewsSourceViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView)

    private val differCallBack = object : DiffUtil.ItemCallback<Source>() {
        override fun areItemsTheSame(oldItem: Source, newItem: Source): Boolean {
            return oldItem.url == newItem.url
        }

        override fun areContentsTheSame(oldItem: Source, newItem: Source): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this,differCallBack)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsSourceViewHolder {
        return NewsSourceViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_source_preview,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: NewsSourceViewHolder, position: Int) {
        val source = differ.currentList[position]

        holder.itemView.apply {
            tvNewsSource.text = source.name
            tvNewsCategory.text = source.category.substring(0, 1).uppercase() + source.category.substring(1).lowercase();
            tvUrl.text = source.url
            setOnClickListener {
                onItemClickListener?.let { it(source) }
            }
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    private var onItemClickListener:((Source) -> Unit)? = null

    fun setOnItemClickListener(listener: (Source) -> Unit) {
        onItemClickListener = listener
    }
}