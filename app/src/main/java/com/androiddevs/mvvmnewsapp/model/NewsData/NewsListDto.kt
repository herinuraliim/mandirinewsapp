package com.androiddevs.mvvmnewsapp.model.NewsData

import com.androiddevs.mvvmnewsapp.model.NewsData.Article

data class NewsListDto(
    val articles: MutableList<Article>,
    val status: String,
    val totalResults: Int
)