package com.androiddevs.mvvmnewsapp.model.NewsSources

data class NewsSourceListDto(
    val sources: List<Source>,
    val status: String
)