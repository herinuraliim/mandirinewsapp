package com.androiddevs.mvvmnewsapp.model.NewsData

data class Source(
    val id: String,
    val name: String
)