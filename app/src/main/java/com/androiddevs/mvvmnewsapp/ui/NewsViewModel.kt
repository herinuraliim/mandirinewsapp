package com.androiddevs.mvvmnewsapp.ui

import android.app.Application
import android.net.ConnectivityManager
import android.net.NetworkCapabilities.TRANSPORT_CELLULAR
import android.net.NetworkCapabilities.TRANSPORT_ETHERNET
import android.net.NetworkCapabilities.TRANSPORT_WIFI
import android.os.Build
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.androiddevs.mvvmnewsapp.aplication.NewsApplication
import com.androiddevs.mvvmnewsapp.core.Resource
import com.androiddevs.mvvmnewsapp.model.NewsData.Article
import com.androiddevs.mvvmnewsapp.model.NewsData.NewsListDto
import com.androiddevs.mvvmnewsapp.model.NewsSources.NewsSourceListDto
import com.androiddevs.mvvmnewsapp.repository.NewsRepository
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class NewsViewModel(
    application : Application,
    val newsRepository: NewsRepository
) : AndroidViewModel(application) {
    val homeNews: MutableLiveData<Resource<NewsListDto>> = MutableLiveData()
    var homeNewsPage = 1
    var homeNewsResponse: NewsListDto? = null

    val searchNews: MutableLiveData<Resource<NewsListDto>> = MutableLiveData()
    var searchNewsPage = 1
    var searchNewsResponse: NewsListDto? = null

    val categoryNews: MutableLiveData<Resource<NewsListDto>> = MutableLiveData()
    var categoryNewsPage = 1
    var categoryNewsResponse : NewsListDto? = null
    var categoryNewsQuery = ""

    val sourceNews: MutableLiveData<Resource<NewsSourceListDto>> = MutableLiveData()
    val sourceNewsPage = 1

    init {
        getHomeNews("us")
        getSourceNews()
    }

    fun getHomeNews(countryCode: String) = viewModelScope.launch {
        safeGetHomeNewsData(countryCode)
    }

    private fun homeNewsResponse(response: Response<NewsListDto>) : Resource<NewsListDto> {
        if(response.isSuccessful) {
            response.body()?.let { resultResponse ->
                homeNewsPage++
                if(homeNewsResponse == null) {
                    homeNewsResponse = resultResponse
                    homeNewsPage = 1
                } else {
                    val oldArticles = homeNewsResponse?.articles
                    val newArticles = resultResponse.articles
                    oldArticles?.addAll(newArticles)
                }
                return Resource.Success(homeNewsResponse ?: resultResponse)
            }
        }
        return  Resource.Error(response.message())
    }

    fun searchNews(query: String) = viewModelScope.launch {
        safeGetSearchNewsData(query)
    }

    private fun searchNewsResponse(response: Response<NewsListDto>) : Resource<NewsListDto> {
        if(response.isSuccessful) {
            response.body()?.let { resultResponse ->
                searchNewsPage++
                if(searchNewsResponse == null) {
                    searchNewsResponse = resultResponse
                    searchNewsPage = 1
                } else {
                    val oldArticles = searchNewsResponse?.articles
                    val newArticles = resultResponse.articles
                    oldArticles?.addAll(newArticles)
                }
                return Resource.Success(searchNewsResponse ?: resultResponse)
            }
        }
        return  Resource.Error(response.message())
    }

    fun clearSearchResults() {
        searchNewsPage = 1
        searchNewsResponse = null
        searchNews.postValue(null)
    }

    fun clearHomeNews() {
        homeNewsPage = 1
        homeNewsResponse = null
        homeNews.postValue(null)
    }

    fun getSourceNews() = viewModelScope.launch {
        sourceNews.postValue(Resource.Loading())
        val response = newsRepository.getSourceNews(sourceNewsPage)
        sourceNews.postValue(sourceNewsResponse(response))
    }

    private fun sourceNewsResponse(response: Response<NewsSourceListDto>) : Resource<NewsSourceListDto> {
        if(response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Resource.Success(resultResponse)
            }
        }
        return  Resource.Error(response.message())
    }

    fun getNewsCategory(source: String, query: String) = viewModelScope.launch {
        categoryNews.postValue(Resource.Loading())
        val response = newsRepository.getNewsCategory(source, categoryNewsPage, query)
        categoryNews.postValue(newsCategoryResponse(response))
    }

    private fun newsCategoryResponse(response: Response<NewsListDto>) : Resource<NewsListDto> {
        if(response.isSuccessful) {
            response.body()?.let { resultResponse ->
                categoryNewsPage++
                if(categoryNewsResponse == null) {
                    categoryNewsResponse = resultResponse
                    categoryNewsPage = 1
                } else {
                    val oldArticles = categoryNewsResponse?.articles
                    val newArticles = resultResponse.articles
                    oldArticles?.addAll(newArticles)
                }
                return Resource.Success(categoryNewsResponse ?: resultResponse)
            }
        }
        return  Resource.Error(response.message())
    }

    //clear data category news
    fun clearCategoryNews() {
        categoryNewsPage = 1
        categoryNewsResponse = null
        categoryNews.postValue(null)
    }

    fun saveNewsArticle(article: Article) = viewModelScope.launch {
        newsRepository.upsert(article)
    }

    fun getFavoriteNews() = newsRepository.getFavoriteNews()

    fun deleteNewsArticle(article: Article) = viewModelScope.launch {
        newsRepository.deleteFavoriteNews(article)
    }

    private suspend fun safeGetSearchNewsData(query: String) {
        searchNews.postValue(Resource.Loading())
        try {
            if(hasInternetConnection()) {
                val response = newsRepository.searchNews(query, searchNewsPage)
                searchNews.postValue(searchNewsResponse(response))
            } else {
                searchNews.postValue(Resource.Error("Ups, No internet connection"))
            }
        } catch(t: Throwable) {
            when(t) {
                is IOException -> searchNews.postValue(Resource.Error("Error"))
                else -> searchNews.postValue(Resource.Error("Conversion Error"))
            }
        }
    }
    private suspend fun safeGetHomeNewsData(countryCode: String) {
        homeNews.postValue(Resource.Loading())
        try {
            if(hasInternetConnection()) {
                val response = newsRepository.getHomeNews(countryCode, homeNewsPage)
                homeNews.postValue(homeNewsResponse(response))
            } else {
                homeNews.postValue(Resource.Error("Ups, No internet connection"))
            }
        } catch(t: Throwable) {
            when(t) {
                is IOException -> homeNews.postValue(Resource.Error("Error"))
                else -> homeNews.postValue(Resource.Error("Conversion Error"))
            }
        }
    }

    private fun hasInternetConnection() : Boolean {
        val connectivityManager = getApplication<NewsApplication>().getSystemService(
            Application.CONNECTIVITY_SERVICE
        ) as ConnectivityManager

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val activeNetwork = connectivityManager.activeNetwork ?: return false
            val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork)
                ?: return false

            return when {
                capabilities.hasTransport(TRANSPORT_WIFI) -> true
                capabilities.hasTransport(TRANSPORT_CELLULAR) -> true
                capabilities.hasTransport(TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.activeNetworkInfo?.run {
                return when(type) {
                    ConnectivityManager.TYPE_WIFI -> true
                    ConnectivityManager.TYPE_ETHERNET -> true
                    ConnectivityManager.TYPE_MOBILE -> true
                    else -> false
                }
            }
        }
        return false
    }

}