package com.androiddevs.mvvmnewsapp.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.androiddevs.mvvmnewsapp.R
import com.androiddevs.mvvmnewsapp.adapter.NewsAdapter
import com.androiddevs.mvvmnewsapp.adapter.NewsSourceAdapter
import com.androiddevs.mvvmnewsapp.core.Resource
import com.androiddevs.mvvmnewsapp.ui.NewsActivity
import com.androiddevs.mvvmnewsapp.ui.NewsViewModel
import kotlinx.android.synthetic.main.fragment_breaking_news.paginationProgressBar
import kotlinx.android.synthetic.main.fragment_breaking_news.rvBreakingNews
import kotlinx.android.synthetic.main.fragment_source_news.rvSourceNews

class SourceNewsFragment : Fragment(R.layout.fragment_source_news){
    lateinit var viewModel: NewsViewModel
    lateinit var sourceNewsAdapter: NewsSourceAdapter
    val TAG = "SourceNewsGragment"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        showProgressBar()
        super.onViewCreated(view, savedInstanceState)

        viewModel = (activity as NewsActivity).viewModel
        setupRecycleView()

        sourceNewsAdapter.setOnItemClickListener {
            val bundle = Bundle().apply {
                putSerializable("source", it)
                viewModel.clearCategoryNews()
                viewModel.getNewsCategory(it.id, "")
            }
            findNavController().navigate(
                R.id.action_sourceNewsFragment_to_categoryNewsFragment,
                bundle
            )
        }

        viewModel.sourceNews.observe(viewLifecycleOwner, Observer { response ->
            when(response) {
                is Resource.Success -> {
                    hideProgressBar()
                    response.data?.let {newsResponse ->
                        sourceNewsAdapter.differ.submitList(newsResponse.sources)
                    }
                }
                is Resource.Error -> {
                    hideProgressBar()
                    response.message?.let {message ->
                        Log.e(TAG, "Error when get data from newsapi.org")
                    }
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }

    private fun hideProgressBar() {
        paginationProgressBar.visibility = View.INVISIBLE
    }

    private fun showProgressBar() {
        paginationProgressBar.visibility = View.VISIBLE
    }

    private fun setupRecycleView() {
        sourceNewsAdapter = NewsSourceAdapter()
        rvSourceNews.apply {
            adapter = sourceNewsAdapter
            layoutManager = LinearLayoutManager(activity)
        }
    }
}