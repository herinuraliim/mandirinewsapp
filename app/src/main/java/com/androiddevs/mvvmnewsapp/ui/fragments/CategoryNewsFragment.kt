package com.androiddevs.mvvmnewsapp.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AbsListView
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androiddevs.mvvmnewsapp.R
import com.androiddevs.mvvmnewsapp.adapter.NewsAdapter
import com.androiddevs.mvvmnewsapp.core.Base_Config
import com.androiddevs.mvvmnewsapp.core.Resource
import com.androiddevs.mvvmnewsapp.ui.NewsActivity
import com.androiddevs.mvvmnewsapp.ui.NewsViewModel
import kotlinx.android.synthetic.main.fragment_breaking_news.paginationProgressBar
import kotlinx.android.synthetic.main.fragment_breaking_news.rvBreakingNews
import kotlinx.android.synthetic.main.fragment_category_news.categorySearch
import kotlinx.android.synthetic.main.fragment_search_news.etSearch
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class CategoryNewsFragment : Fragment(R.layout.fragment_category_news) {
    lateinit var viewModel: NewsViewModel
    lateinit var newsAdapter: NewsAdapter
    var query = ""
    val args: CategoryNewsFragmentArgs by navArgs()
    val TAG = "CategoryNewsFragment"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //clear data category news
        showProgressBar()
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as NewsActivity).viewModel
        setupRecycleView()

        newsAdapter.setOnItemClickListener {
            val bundle = Bundle().apply {
                putSerializable("article", it)
            }
            findNavController().navigate(
                R.id.action_categoryNewsFragment_to_articleNewsFragment,
                bundle
            )
        }

        val source = args.source
        var job : Job? = null

        categorySearch.addTextChangedListener { editable ->
            job?.cancel()
            job = MainScope().launch {
                delay(Base_Config.SEARCH_DELAY)
                editable?.let {
                    if(editable.toString().isNotEmpty()) {
                        viewModel.clearCategoryNews()
                        query = editable.toString()
                        viewModel.getNewsCategory(source.id, editable.toString())
                    } else {
                        //delete all data in recycle view
                        viewModel.clearCategoryNews()
                        viewModel.getNewsCategory(source.id, "")
                    }
                }
            }
        }

        viewModel.categoryNews.observe(viewLifecycleOwner, Observer { response ->
            when(response) {
                is Resource.Success -> {
                    hideProgressBar()
                    response.data?.let {newsResponse ->
                        newsAdapter.differ.submitList(newsResponse.articles)
                    }
                }
                is Resource.Error -> {
                    hideProgressBar()
                    response.message?.let {message ->
                        Log.e(TAG, "Error when get data from newsapi.org")
                        Toast.makeText(activity, "Ups, There is an Error: $message", Toast.LENGTH_LONG).show()

                    }
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }

    private fun hideProgressBar() {
        paginationProgressBar.visibility = View.INVISIBLE
    }

    private fun showProgressBar() {
        paginationProgressBar.visibility = View.VISIBLE
    }

    var isLoading = false;
    var isLastPage = false;
    var isScrolling = false;

    val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                isScrolling = true
            }
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val layoutManager = recyclerView.layoutManager as LinearLayoutManager
            val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
            val visibleItemCount = layoutManager.childCount
            val totalItem = layoutManager.itemCount

            val isNotLoadingAndLast = !isLoading && !isLastPage
            val isAtLastItem = firstVisibleItemPosition + visibleItemCount >= totalItem
            val isNotAtBeginning = firstVisibleItemPosition >= 0
            val isTotalMoreThanVisible = totalItem >= Base_Config.QUERY_PAGE_SIZE
            val shouldPaginate = isNotLoadingAndLast && isAtLastItem && isNotAtBeginning && isTotalMoreThanVisible && isScrolling

            val source = args.source

            if(shouldPaginate) {
                viewModel.getNewsCategory(source.id, query)
                isScrolling = false
            }
        }
    }

    private fun setupRecycleView() {
        newsAdapter = NewsAdapter()
        rvBreakingNews.apply {
            adapter = newsAdapter
            layoutManager = LinearLayoutManager(activity)
            addOnScrollListener(this@CategoryNewsFragment.scrollListener)
        }
    }
}