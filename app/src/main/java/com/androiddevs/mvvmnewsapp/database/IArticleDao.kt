package com.androiddevs.mvvmnewsapp.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.androiddevs.mvvmnewsapp.model.NewsData.Article

@Dao
interface IArticleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(article: Article): Long

    @Query("SELECT * FROM newsArticle")
    fun getAllNewsArticle(): LiveData<List<Article>>

    @Delete
    fun deleteNewsArticle(article: Article)
}