package com.androiddevs.mvvmnewsapp.repository

import com.androiddevs.mvvmnewsapp.core.Base_Config.Companion.API_KEY
import com.androiddevs.mvvmnewsapp.core.Base_Config.Companion.QUERY_PAGE_SIZE
import com.androiddevs.mvvmnewsapp.model.NewsData.NewsListDto
import com.androiddevs.mvvmnewsapp.model.NewsSources.NewsSourceListDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface INewsApi {
    @GET("v2/top-headlines/sources")
    suspend fun GetNewsSource(
        @Query("page")
        pageNumber: Int = 1,
        @Query("apiKey")
        apiKey: String = API_KEY
    ): Response<NewsSourceListDto>

    @GET("v2/top-headlines")
    suspend fun GetNewsDataCategory(
        @Query("sources")
        source: String = "abc-news",
        @Query("q")
        query: String = "",
        @Query("page")
        pageNumber: Int = 1,
        @Query("pageSize")
        pageSize: Int = QUERY_PAGE_SIZE,
        @Query("apiKey")
        apiKey: String = API_KEY
    ): Response<NewsListDto>

    @GET("v2/top-headlines")
    suspend fun GetNewsData(
        @Query("country")
        countryCode: String = "us",
        @Query("page")
        pageNumber: Int = 1,
        @Query("pageSize")
        pageSize: Int = QUERY_PAGE_SIZE,
        @Query("apiKey")
        apiKey: String = API_KEY
    ): Response<NewsListDto>

    @GET("v2/everything")
    suspend fun SearchNews(
        @Query("q")
        query: String,
        @Query("page")
        pageNumber: Int = 1,
        @Query("pageSize")
        pageSize: Int = QUERY_PAGE_SIZE,
        @Query("apiKey")
        apiKey: String = API_KEY
    ): Response<NewsListDto>
}