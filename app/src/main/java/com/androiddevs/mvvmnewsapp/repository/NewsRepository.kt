package com.androiddevs.mvvmnewsapp.repository

import com.androiddevs.mvvmnewsapp.controller.RetrofitInstance
import com.androiddevs.mvvmnewsapp.database.ArticleDatabase
import com.androiddevs.mvvmnewsapp.model.NewsData.Article

class NewsRepository(
    val db: ArticleDatabase
) {
    suspend fun getHomeNews(countryCode: String, pageNumber: Int) =
        RetrofitInstance.api.GetNewsData(countryCode, pageNumber)

    suspend fun getNewsCategory(source: String, pageNumber: Int, query: String) =
        RetrofitInstance.api.GetNewsDataCategory(source, query, pageNumber)

    suspend fun searchNews(query : String, pageNumber: Int) =
        RetrofitInstance.api.SearchNews(query, pageNumber)

    suspend fun getSourceNews(pageNumber: Int) =
        RetrofitInstance.api.GetNewsSource(pageNumber)

    suspend fun upsert(article: Article) =
        db.getNewsArticleDao().upsert(article)

    fun getFavoriteNews() =
        db.getNewsArticleDao().getAllNewsArticle()

    suspend fun deleteFavoriteNews(article: Article) =
        db.getNewsArticleDao().deleteNewsArticle(article)
    }