package com.androiddevs.mvvmnewsapp.core

class Base_Config {
    companion object {
        //Limit access to the API
        const val API_KEY_OLD = "bf7c0096b7964bd9aeb0d6d73cd062b4"
        const val API_KEY = "786badd122df4f7682e025cb08d12862"
        const val PARENT_URL = "https://newsapi.org/"
        const val SEARCH_DELAY = 500L
        const val QUERY_PAGE_SIZE = 10
        const val MESSAGE_DATABASE_SUCCESS_INSERT = "Favorite News saved successfully"
        const val MESSAGE_DATABASE_SUCCESS_DELETE = "Successfully deleted Favorite News Article"
    }
}